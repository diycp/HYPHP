<?php
return array(
	'syntax error, unexpected'=>'语法错误, 意外',
	'Call to undefined function'=>'调用了未定义的函数',
	'failed to open stream: Invalid argument'=>'未能打开流：无效的参数',
	'Undefined variable'=>'未定义的变量',
	'Undefined offset'=>'未定义的偏移 (数组索引不存在)',
	'Illegal string offset'=>'非法的字符串偏移 (数组索引不存在)',
	'Cannot access empty property'=>'不能访问空属性',
);